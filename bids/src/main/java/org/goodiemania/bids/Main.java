package org.goodiemania.bids;

import io.javalin.Javalin;
import io.javalin.validation.JavalinValidation;

import java.time.Instant;

public class Main {
    public static void main(String[] args) {
        BidFinder bidFinder = new BidFinder();
        Javalin app = Javalin.create().start(8081);
        JavalinValidation.register(Instant.class, v -> Instant.ofEpochMilli(Long.valueOf(v)));
        app.post("/bids", ctx -> ctx.result(bidFinder.findMax(ctx.body())));
        app.post("/*", ctx -> {
            ctx.status(404);
            ctx.json("Oh boy this is awkward");
        });
    }
}
