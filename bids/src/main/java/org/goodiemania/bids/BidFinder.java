package org.goodiemania.bids;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Comparator;

public class BidFinder {
    public String findMax(final String content) {
        //these first 2-3 lines are a dirty hack because apparently the new lines are being eaten.
        return Arrays.stream(content.split("BID_AM"))
                .filter(StringUtils::isNotBlank)
                .map(s -> "BID_AM" + s)
                .map(Bid::new)
                .max(Comparator.comparingInt(Bid::getAmount))
                .orElseThrow(IllegalStateException::new)
                .getBid();
    }

    private class Bid {
        private Integer amount;
        private String bid;

        Bid(final String bid) {
            this.setBid(bid);
            this.setAmount(new Integer(bid.substring(bid.indexOf("=") + 1, bid.indexOf(";", 0))));
        }


        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public String getBid() {
            return bid;
        }

        public void setBid(String bid) {
            this.bid = bid;
        }
    }
}
