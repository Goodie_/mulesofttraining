package com.mulesoft.training;

import org.mule.api.MuleEventContext;
import org.mule.api.MuleException;
import org.mule.api.lifecycle.Callable;
import org.mule.api.lifecycle.Startable;

public class MuleLifeCycleExample implements Startable, Callable {

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		// TODO Auto-generated method stub

		System.out.println("MuleLifeCycleExample callable called");	
		return eventContext.getMessage();
	}

	@Override
	public void start() throws MuleException {
		System.out.println("MuleLifeCycleExample component started");	
	}

}
