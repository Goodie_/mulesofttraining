package com.mulesoft.training;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mule.api.annotations.param.InboundHeaders;
import org.mule.api.annotations.param.Payload;

public class MuleComponent {

	private int count = 0;
	
  public Map<String,String> processMap(final Map<String, String> inputMap) {
    // processMap implementation
	  inputMap.put("Hello there", "General Kenobi");
    return inputMap;
  }

  public Map<String,String> processArray(final List<String> inputList) {
    // processArray implementation
	final Map<String, String> outputMap = new HashMap<>();
	outputMap.put("message", inputList.get(0));
	outputMap.put("I have the high ground", "Anakin");
    return outputMap;
  }

  public Map<String,String> processString(final String inputString) {
    // processString implementation
		final Map<String, String> outputMap = new HashMap<>();
		outputMap.put("message", inputString);
		outputMap.put("My new Empire", "MY NEW EMPIRE");
	    return outputMap;
  }

  public Map<String,String> processAll(@Payload final Object input, @InboundHeaders("http.method") final String httpMethod) {
    // processString2 implementation

	count = count + 1;
		
	final Map<String, String> outputMap = new HashMap<>();
	outputMap.put("message", input.toString());
	outputMap.put("method", httpMethod);
	outputMap.put("count", Integer.toString(count));
	outputMap.put("My new Empire", "WILL BRING PEACE TO THE GALAXY");
    return outputMap;
  }

} 
